function component_create_product() {
    document.getElementById("createProduct").innerHTML = `
    <h3>สินค้า</h3>
    <div class="card">
        <div class="card-header">
            <h4>เพิ่มสินค้า:</h4>
        </div>
        <div class="card-body">
            <form>
                <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label-lg">รหัสสินค้า</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputPassword">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label-lg">รายละเอียดสินค้า</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputPassword">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label-lg">หมวดหมู่สินค้า</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputPassword">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label-lg">ยี่ห้อ</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputPassword">
                    </div>
                </div>
                <div class="text-right mb-3">
                <button type="button" class="btn btn-warning ">บันทึก</button>
                </div>
            </form>
        </div>
    </div>
    `;
}

component_create_product()