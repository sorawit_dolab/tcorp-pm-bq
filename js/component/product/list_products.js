function component_list_products() {
    var myArray    = new Array();
    myArray[0] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "-", "10%", "เสร็จสมบูรณ์", "ก่อนเซ็นสัญญา"];
    myArray[1] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "10%", "เสร็จสมบูรณ์", "ยื่นเสนอราคา"];
    myArray[2] = ["T1902", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "20%", "เสร็จสมบูรณ์", "ลดราคาครั้งที่ 1"];
    myArray[3] = ["T1903", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "30%", "เสร็จสมบูรณ์", "ยืนยันราคา"];
    myArray[4] = ["T1904", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "40%", "เสร็จสมบูรณ์", "เซ็นสัญญา"];
    myArray[5] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "-", "10%", "เสร็จสมบูรณ์", "หลังเซ็นสัญญา"];
        
    var myTable= `
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>รหัสสินค้า</th>
                    <th>รายละเอียด</th>
                    <th>หมวดหมู่สินค้า</th>
                    <th>ยี่ห้อ</th>
                    <th>ดำเนินการ</th>
                </tr>
            </thead>
            <tbody>
            `;
            for (var i=0; i<myArray.length; i++) {
                var index_row = i + 1;
                myTable+=`
                    <tr>
                        <td>` + index_row + `</td>
                        <td>H80Z4MABC</td>
                        <td>ETSI Service Shelf,48V/60V,4-Fan</td>
                        <td>สายไฟ</td>
                        <td>Yamazaki</td>
                        <td>
                            <a href="#" class="btn btn-danger">ลบ</a>
                            <a href="edit-project-detail.html" class="btn btn-warning">แก้ไข</a>
                        </td>
                    </tr>
                `;
            }
          
        myTable += `
            </tbody>
        </table>
        `
    document.getElementById('listProducts').innerHTML = myTable;
}

component_list_products()


function search_product() {
    var search_box = `
        <div class="row">
            <div class="col-9">
                <div class="dropdown">
                    <a class="btn btn-secondary dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        ตัวกรอง
                    </a>
                    
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">หมวดหมู่</a>
                    </div>
                </div>
            </div>
            <div class="col-3">
                <form class="">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="ค้นหา">
                </form>
            </div>
        </div>

        
    `
    document.getElementById('searchTextbox').innerHTML = search_box;

}

search_product()