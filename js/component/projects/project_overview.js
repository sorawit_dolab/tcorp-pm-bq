function componentProjectOverview() {
    document.getElementById("projectsOverview").innerHTML = `
    <div class="row">
        <div class="col-12">
            <h1 class="T-Corporation-CoLt">T Corporation Co.,Lts</h1>
            <h2 class="Projects line_under_Projects">Projects</h2>
            <i class="fas fa-sliders-h" style="font-size:12px;"></i><span class="Filter">Filter</span>
            <i class="fas fa-sort-amount-down-alt" style="font-size:12px;"></i><span class="Sort">Sort</span>
        </div>
    </div>
    `;
}


function generateProjectsOverview() {
    var myArray    = new Array();
      myArray[0] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "info", "10%", "TOT", "#e33232"];
      myArray[1] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "warning", "80%", "CAT", "#3cb371"];
      myArray[2] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
      myArray[3] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
      myArray[4] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "danger", "40%", "CAT", "#ffa500"];

    //   myArray[5] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "info", "10%", "TOT", "#e33232"];
    //   myArray[6] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "warning", "80%", "CAT", "#3cb371"];
    //   myArray[7] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
    //   myArray[8] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
    //   myArray[9] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "danger", "40%", "CAT", "#ffa500"];

    //   myArray[10] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "info", "10%", "TOT", "#e33232"];
    //   myArray[11] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "warning", "80%", "CAT", "#3cb371"];
    //   myArray[12] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
    //   myArray[13] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
    //   myArray[14] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "danger", "40%", "CAT", "#ffa500"];

    //   myArray[15] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "info", "10%", "TOT", "#e33232"];
    //   myArray[16] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "warning", "80%", "CAT", "#3cb371"];
    //   myArray[17] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
    //   myArray[18] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "100%", "TOT", "#3cb371"];
    //   myArray[19] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "danger", "40%", "CAT", "#ffa500"];
      
    var myTable= `
        <div class="row">

    `;
    for (var i=0; i<myArray.length; i++) {
      myTable+=`
        <div class="card" href="_project_tasks.html">
          <div class="small-card" style="background-color:` + myArray[i][5] + ` "> 
            <h1 class="Percent"><b>` + myArray[i][3] + ` </b></h1>
            <p class="Progress">Progress</p>
          </div>
          <h1 class="Card-setting">ติดตั้งอุปกรณ์</h1>
          <p class="Card-sign">` + myArray[i][4] + ` <span class="Card-id"> :` + myArray[i][0] + ` </span> </p>
          <div class="card-body">
            <div class="row">
              <div class="col-5">
                <p class="Class-info">งานที่สำเร็จล่าสุด</p>
                <p class="Class-info-status" style="color: #11be9f;">ยื่นงาน</p>
              </div>
              <div class="col-5">
                <p class="Class-info">งานถัดไปที่ต้องทำ</p>
                <p class="Class-info-status" style="color: #be1111;">ยืนยันราคา</p>
              </div>
            </div>
            <div class="row">
              <div class="col-5">
                <p class="Class-info">งานที่สำเร็จล่าสุด</p>
                <p class="Class-info-status" style="color: #11be9f;">ยื่นงาน</p>
              </div>
            </div>
            <div class="row">
              <div class="col-5">
                <span class="Class-info">สถานะ:<span class="Class-info-identity">ตามแผน</span></span>
              </div>
              <div class="col-7">
                <span class="Class-info" style="color: #000000;">ส่งมอบงานภายใน:<span class="Class-info-identity" style="color: #000000;">30 ธ.ค. 2562</span></span>
              </div>
            </div> 

            <div class="progress card-project-prgress-bar">
              <div class="progress-bar bg-` + myArray[i][2] + `" role="progressbar" style="width: ` + myArray[i][3] + `" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
          </div>
          <a href="_project_tasks.html" class="stretched-link"></a>

        </div>
        `;
    }  
    myTable += `

    </div>
    `
    document.getElementById('tableTasksStatus').innerHTML = myTable;
}

componentProjectOverview();
generateProjectsOverview();