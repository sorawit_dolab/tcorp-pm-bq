function saveValueAuto(ev){
    console.log("ev", ev);
}
function myFunction(ev) {
    // console.log("ev", ev);
    // console.log("ev.parentElement", ev.target.parentElement);
    // console.log("ev.target", ev.target.parentElement.parentElement.rowIndex);
    // console.log("a", ev.target.parentElement);
    seleteRowIndex = ev.target.parentElement.parentElement.rowIndex;

    // console.log("seleteRowIndex", seleteRowIndex);
    var taskTable = document.getElementById("display-table");
    var taskRow = taskTable.getElementsByTagName("tr");
    // console.log("ev.target", taskRow[1].childNodes[1].innerHTML);
    // console.log("ev.target", taskRow);
    // console.log("ev.taskTable", taskTable);
    

    if (ev.target.className !== "fas fa-caret-right icon-movement"){
        // console.log("className: right", taskRow.length)
        ev.target.parentElement.innerHTML = `<td><i class="fas fa-caret-right icon-movement" onclick="myFunction(event)"></i></td>`
        for (var i=1; i<taskRow.length; i++) {
            // console.log("i", i)
            // console.log("i", seleteRowIndex+1)
            var hearder = taskRow[i].childNodes[1].innerHTML;
            if (i >= seleteRowIndex+1){
                // console.log("i", i)
                // console.log(">>>seleteRowIndex+1", i)
                if (hearder === '<i class="fas fa-grip-vertical icon"></i>'){
                    console.log(">>> break vertical", i)
                    break;
                }
                else if (hearder === '<i class="fas fa-grip-down icon"></i>') {
                    console.log(">>> break down", i)
                    break;
                }
                taskRow[i].style.display = 'none';
            }
            
        }
    }
    else{
        // console.log("className: down")
        ev.target.parentElement.innerHTML = `<td><i class="fas fa-caret-down icon-movement" onclick="myFunction(event)"></i></td>`
        for (var i=1; i<taskRow.length; i++) {
            if (i !== seleteRowIndex) {
                taskRow[i].style.display = '';
            }
            
        }

    }
}

function componentListBOQ() {
    var myArray    = new Array();
      myArray[0] = ["1. MA5600T", "-", "6", "2", "-", "10%", "เสร็จสมบูรณ์", "ก่อนเซ็นสัญญา"];
      myArray[1] = ["H80Z4MABC", "ETSI Service Shelf,48V/60V,4-Fan", "12", "22", "0", "10%", "เสร็จสมบูรณ์", "ยื่นเสนอราคา"];
      myArray[2] = ["H80D00SCUN02", "Super Control Unit Board", "40", "30", "10", "20%", "เสร็จสมบูรณ์", "ลดราคาครั้งที่ 1"];
      myArray[3] = ["H80D00X2CS01", "2-port 10GE Uplink Interface Card,support SyncE", "206", "200", "6", "30%", "เสร็จสมบูรณ์", "ยืนยันราคา"];
      myArray[4] = ["H80-PRTE", "Connect Power Board", "11", "11", "0", "40%", "เสร็จสมบูรณ์", "เซ็นสัญญา"];
      myArray[5] = ["C0DF2D200", "Single Cable,DC Feeder Cable,2.2m,2*T6^2B,H07Z-K-6^2BL+H07Z-K-6^2B,D3F-2S", "235", "400", "0", "10%", "เสร็จสมบูรณ์", "หลังเซ็นสัญญา"];
      myArray[6] = ["H80XBPMCBS00", "Blank Panel for Main Control Board Slot (Shielded)", "66", "100", "0", "40%", "กำลังดำเนินการ", "Kick off"];
      myArray[7] = ["H80XBPSPLB00", "Blank Panel for Service Board And Splitter Board Slot (Shielded)", "11", "25", "0", "40%", "รอ", "ขออนุมัติ PAT"];
      myArray[8] = ["2. Installation", "-", "22", "", "40%", "รอ", "ขอเข้าสำรวจพื้นที่"];
      myArray[9] = ["-", "Installation and Commissioning For MA5600T", "1", "-", "-", "40%", "รอ", "ส่งของถึง TCORP"];
      myArray[10] = ["-", "Site preparation", "1", "-", "-", "40%", "รอ", "ส่งของถึง ลูกค้า"];
        
      var myTable= `
        <div class="row">
          <table id="display-table">

          <thead>
              <tr>
              <th>&nbsp; </th>
              <th>&nbsp;&nbsp;</th>
              <th>โมเดล (Model)</th>
              <th>รายละเอียด</th>
              <th class="input-center">ชุมสายเอกชัย</th>
              <th class="input-center">จำนวน (Qty.)</th>
              <th class="input-center">Stock</th>
              <th class="input-center">ต้องเปิด PO</th>
              <th>วันที่รับของ</th>
              <th>จัดเก็บลง Stock</th>
              <th>วันที่ส่งของให้ลูกค้า</th>
              </tr>
          </thead>

          <tbody>
          `;
          
      for (var i=0; i<myArray.length; i++) {
        if (i===0 || i===8){
          myTable+=`
            <tr ondrop="drop(event)" ondragover="allowDrop(event)">
                <td draggable="true" ondragstart="drag(event)"><i class="fas fa-grip-vertical icon"></i></td>
                <td><i class="fas fa-caret-down icon-movement" onclick="myFunction(event)"></i></td>
                <td colspan="11"><input type="text" class="form-control input-task" onchange="saveValueAuto(event)" value="` + myArray[i][0] + `" id="` + myArray[i][0] + `" /></td>
            </tr>
          `;
        }
        else{
          myTable+=`
              <tr ondrop="drop(event)" ondragover="allowDrop(event)">
                <td draggable="true" ondragstart="drag(event)">
                    <i class="fas fa-grip-vertical icon"></i>
                </td>
                <td></td>
                  <td><input type="text" class="form-control input-task" onchange="saveValueAuto()" value="` + myArray[i][0] + `" id="` + myArray[i][0] + `" /></td>
                  
                  <td><input type="text" onchange="saveValueAuto()" class="form-control table-col-width-toolong input-task" value="` + myArray[i][1] + `" id="` + myArray[i][1] + `" /></td>
                  <td><input type="text" onchange="saveValueAuto()" class="form-control table-col-width-long input-center input-task" value="` + myArray[i][2] + `" id="` + myArray[i][2] + `" /></td>
                  <td><input type="text" onchange="saveValueAuto()" class="form-control table-col-width-short input-center input-task" value="` + myArray[i][2] + `" id="` + myArray[i][2] + `" /></td>
                  <td><input type="text" onchange="saveValueAuto()" class="form-control table-col-width-short input-center input-task" value="` + myArray[i][3] + `" id="` + myArray[i][3] + `" /></td>
  
                  <td><input type="text" onchange="saveValueAuto()" class="form-control table-col-width-long input-center input-task" value="` + myArray[i][4] + `" id="` + myArray[i][4] + `" /></td>
                  <td><input type="date" name="bday" value="2019-11-05" class="form-control table-col-width-long input-task"></td>
                  
                  <td class="table-col-width-long">
                    <select class="form-control form-control-lg">
                      <option>รอ</option>
                      <option>จัดเก็บลง Stock แล้ว</option>
                      <option>เตรียมจัดส่งสินค้า</option>
                      <option>พร้อมส่งสินค้าแล้ว</option>
                      <option>ส่งสินค้าเรียบร้อยแล้ว</option>
                    </select>
                  </td>
                  <td><input type="date" name="bday" value="2019-11-05"  class="form-control table-col-width-long input-task"></td>
                  
  

                </div> 
                </td>
              </tr>
          `;
        }
        
      }  
      
      myTable += `
          </tbody>
          </table>
        </div>
      `
      document.getElementById('componentListBOQ').innerHTML = myTable;

}

componentListBOQ()