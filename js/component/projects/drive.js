function saveValueAuto(ev){
    console.log("ev", ev);
}
function myFunction(ev) {
    // console.log("ev", ev);
    // console.log("ev.parentElement", ev.target.parentElement);
    // console.log("ev.target", ev.target.parentElement.parentElement.rowIndex);
    // console.log("a", ev.target.parentElement);
    seleteRowIndex = ev.target.parentElement.parentElement.rowIndex;

    // console.log("seleteRowIndex", seleteRowIndex);
    var taskTable = document.getElementById("display-table");
    var taskRow = taskTable.getElementsByTagName("tr");
    // console.log("ev.target", taskRow[1].childNodes[1].innerHTML);
    // console.log("ev.target", taskRow);
    // console.log("ev.taskTable", taskTable);
    

    if (ev.target.className !== "fas fa-caret-right icon-movement"){
        // console.log("className: right", taskRow.length)
        ev.target.parentElement.innerHTML = `<td><i class="fas fa-caret-right icon-movement" onclick="myFunction(event)"></i></td>`
        for (var i=1; i<taskRow.length; i++) {
            // console.log("i", i)
            // console.log("i", seleteRowIndex+1)
            var hearder = taskRow[i].childNodes[1].innerHTML;
            if (i >= seleteRowIndex+1){
                // console.log("i", i)
                // console.log(">>>seleteRowIndex+1", i)
                if (hearder === '<i class="fas fa-grip-vertical icon"></i>'){
                    console.log(">>> break vertical", i)
                    break;
                }
                else if (hearder === '<i class="fas fa-grip-down icon"></i>') {
                    console.log(">>> break down", i)
                    break;
                }
                taskRow[i].style.display = 'none';
            }
            
        }
    }
    else{
        // console.log("className: down")
        ev.target.parentElement.innerHTML = `<td><i class="fas fa-caret-down icon-movement" onclick="myFunction(event)"></i></td>`
        for (var i=1; i<taskRow.length; i++) {
            if (i !== seleteRowIndex) {
                taskRow[i].style.display = '';
            }
            
        }

    }
}

function componentDrive() {
    var myArray    = new Array();
    myArray[0] = ["หนังสือรับรอง", "คุณโอมห์", "600KB"];
    myArray[1] = ["BOQ Ver 1.0", "คุณโอมห์", "1MB"];
    myArray[2] = ["TOR", "คุณโอมห์", "750KB"];
    
    var myTable= `
    <div class="row">
        <table id="display-table">
        <thead>
            <tr>
            <th>ชื่อไฟท์</th>
            <th>เจ้าของไฟท์</th>
            <th>อัพเดตเมื่อ</th>
            <th>ขนาดไฟท์</th>
            </tr>
        </thead>
    
        <tbody>
        `;
        
    for (var i=0; i<myArray.length; i++) {
        myTable+=`
            <tr onclick="window.location.href = 'pdf/test.pdf';" style= "cursor:pointer;">
                <td><h4>` + myArray[i][0] + `</h4></td> 
                <td><h4>` + myArray[i][1] + `</h4></td>
                <td><h4>2019-11-01</h4></td>
                <td><h4>` + myArray[i][2] + `</h4></td>
            </tr>
        `;
    }  
    
    myTable += `
        </tbody>
        </table>
    </div>
    `
    document.getElementById('componentDrive').innerHTML = myTable;
    
}

componentDrive()