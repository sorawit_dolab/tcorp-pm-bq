function componentProjectTopic() {
    document.getElementById("componentProjectTopic").innerHTML = `
    <div class="row">
      <div class="col-12">
          <h4 class="T-Corporation-CoLt">T Corporation Co.,Lts</h4>
          <h2 class="Projects line_under_Projects">T1908: ติดตั้งอุปกรณ์</h2>
          <h4>โครงการ: งานจ้างเหมาติดตั้งอุปกรณ์ OLT/CARD GPON/Line Card /up Link และอุปกรณ์อื่น ๆ ในพื้นที่ บน.1.1</h4>
          <h4>ลูกค้า: TOT Public Company Limited (TOT)</h4>
          <div class="row">
              <div class="col-4">
                  <h4>มูลค่างาน: 5,305,000.00 บาท</h4>
              </div>
              <div class="col-4">
                  <h4>ส่งมอบงานภายใน: 30 ธ.ค. 2562</h4>
              </div>
              <div class="col-4">
                  <h4>สถานะ: ตามแผน</h4>
              </div>
          </div>
      </div>
      <div class="row">
          <div class="col-12">
              <button type="submit" onclick="window.location.href = '_project_tasks.html';"  class="btn btn-dark">โปรเจค</button>
              <button type="submit" onclick="window.location.href = '_boq.html';" class="btn btn-dark">BOQ</button>
              <button type="submit" onclick="window.location.href = '_drive.html';" class="btn btn-dark">เอกสาร</button>
          </div>
      </div>
  </div>
    `;
}


function generateProjectTasks() {
  var myArray    = new Array();
  myArray[0] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "-", "10%", "เสร็จสมบูรณ์", "ก่อนเซ็นสัญญา"];
  myArray[1] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "10%", "เสร็จสมบูรณ์", "ยื่นเสนอราคา"];
  myArray[2] = ["T1902", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "20%", "เสร็จสมบูรณ์", "ลดราคาครั้งที่ 1"];
  myArray[3] = ["T1903", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "30%", "เสร็จสมบูรณ์", "ยืนยันราคา"];
  myArray[4] = ["T1904", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "success", "40%", "เสร็จสมบูรณ์", "เซ็นสัญญา"];
  myArray[5] = ["T1901", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "-", "10%", "เสร็จสมบูรณ์", "หลังเซ็นสัญญา"];
  myArray[6] = ["T1904", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "warning", "40%", "กำลังดำเนินการ", "Kick off"];
  myArray[7] = ["T1904", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "secondary", "40%", "รอ", "ขออนุมัติ PAT"];
  myArray[8] = ["T1904", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "secondary", "40%", "รอ", "ขอเข้าสำรวจพื้นที่"];
  myArray[9] = ["T1904", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "secondary", "40%", "รอ", "ส่งของถึง TCORP"];
  myArray[10] = ["T1904", "งานจ้างเหมาติดตั้งอุปกรณ์ ในพื้นที่ บน.1.1", "secondary", "40%", "รอ", "ส่งของถึง ลูกค้า"];
    
  var myTable= `
    <div class="row">
      <table id="display-table">
      <thead>
          <tr>
          <th>&nbsp; </th>
          <th>&nbsp;&nbsp;</th>
          <th>ภารกิจ</th>
          <th>ผู้รับผิดชอบ</th>
          <th>กำหนดส่ง</th>
          <th>อัพเดตเมื่อ</th>
          <th>สถานะ</th>
          </tr>
      </thead>
      <tbody>
      `;
      
  for (var i=0; i<myArray.length; i++) {
    if (i===0 || i===5){
      myTable+=`
        <tr ondrop="drop(event)" ondragover="allowDrop(event)">
            <td draggable="true" ondragstart="drag(event)"><i class="fas fa-grip-vertical icon"></i></td>
            <td><i class="fas fa-caret-down icon-movement" onclick="myFunction(event)"></i></td>
            <td colspan="5"><input type="text" class="form-control input-task" onkeyup="createNewRow(event)" onchange="saveValueAuto(event)" value="` + myArray[i][5] + `" id="` + myArray[i][5] + `" /></td>
        </tr>
      `;
    }
    else{
      myTable+=`
          <tr ondrop="drop(event)" ondragover="allowDrop(event)">
            <td draggable="true" ondragstart="drag(event)">
                <i class="fas fa-grip-vertical icon"></i>
            </td>
            <td></td>
              <td><input type="text" class="form-control input-task" onkeyup="createNewRow(event)" onchange="saveValueAuto()" value="` + myArray[i][5] + `" id="` + myArray[i][5] + `" /></td>
              <td>
                <select class="form-control form-control-lg">
                  <option>คุณยุ้ย</option>
                  <option>คุณโอมห์</option>
                  <option>คุณผึ้ง</option>
                  <option>คุณเม้ง</option>
                </select>
              </td>

              <td><input type="date" class="form-control input-task" name="bday" value="2019-11-01"></td>

              <td><input type="date" class="form-control input-task" name="bday" value="2019-11-05"></td>

              <td> <div class="dropdown">
              <button class="btn btn-` + myArray[i][2] + ` dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    ` + myArray[i][4] + `
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#">รอ</a>
                <a class="dropdown-item" href="#">กำลังดำเนินการ</a>
                <a class="dropdown-item" href="#">เสร็จสมบูรณ์</a>
              </div>
            </div> 
            </td>
          </tr>
      `;
    }
    
  }  
  
  myTable += `
      </tbody>
      </table>
    </div>
  `
  document.getElementById('tableTasksStatus').innerHTML = myTable;
}

function saveValueAuto(ev){
  console.log("ev", ev);
}
function myFunction(ev) {
  // console.log("ev", ev);
  // console.log("ev.parentElement", ev.target.parentElement);
  // console.log("ev.target", ev.target.parentElement.parentElement.rowIndex);
  // console.log("a", ev.target.parentElement);
  seleteRowIndex = ev.target.parentElement.parentElement.rowIndex;

  // console.log("seleteRowIndex", seleteRowIndex);
  var taskTable = document.getElementById("display-table");
  var taskRow = taskTable.getElementsByTagName("tr");
  // console.log("ev.target", taskRow[1].childNodes[1].innerHTML);
  // console.log("ev.target", taskRow);
  // console.log("ev.taskTable", taskTable);
  

  if (ev.target.className !== "fas fa-caret-right icon-movement"){
      // console.log("className: right", taskRow.length)
      ev.target.parentElement.innerHTML = `<td><i class="fas fa-caret-right icon-movement" onclick="myFunction(event)"></i></td>`
      for (var i=1; i<taskRow.length; i++) {
          // console.log("i", i)
          // console.log("i", seleteRowIndex+1)
          var hearder = taskRow[i].childNodes[1].innerHTML;
          if (i >= seleteRowIndex+1){
              // console.log("i", i)
              // console.log(">>>seleteRowIndex+1", i)
              if (hearder === '<i class="fas fa-grip-vertical icon"></i>'){
                  console.log(">>> break vertical", i)
                  break;
              }
              else if (hearder === '<i class="fas fa-grip-down icon"></i>') {
                  console.log(">>> break down", i)
                  break;
              }
              taskRow[i].style.display = 'none';
          }
          
      }
  }
  else{
      // console.log("className: down")
      ev.target.parentElement.innerHTML = `<td><i class="fas fa-caret-down icon-movement" onclick="myFunction(event)"></i></td>`
      for (var i=1; i<taskRow.length; i++) {
          if (i !== seleteRowIndex) {
              taskRow[i].style.display = '';
          }
          
      }

  }
}
  
function createNewRow(ev) {
  dragRowIndex = ev.target.parentElement.parentElement.rowIndex;
  if (event.keyCode === 13) {
      event.preventDefault();
      var table = document.getElementById("display-table");
      var row = table.insertRow(dragRowIndex+1);
      var cell = row.insertCell(0);
      cell.colSpan = "5";

      // Set Attribute
      row.draggable = true;
      row.ondragstart = function(e){drag(e)};
      row.ondrop = function(e){drop(e)};
      row.ondragover = function(e){allowDrop(e)};

      // Check and Set Col.
      var cell_idx = []
      console.log("table", ev)
      for (i = 0; i < ev.target.parentElement.parentElement.cells.length; i++) {
          cell_idx.push(row.insertCell(i))
      }

      // Fill data into rows
      cell_idx[0].innerHTML = '<td draggable="true" ondragstart="drag(event)"><i class="fas fa-grip-vertical icon"></i></td>'
      cell_idx[1].innerHTML = '<td></td>'
      cell_idx[2].innerHTML = '<td><input type="text" class="form-control input-task" onkeyup="createNewRow(event)" onchange="saveValueAuto()" value="" id="" /></td>'
      cell_idx[3].innerHTML = `<td>
              <select class="form-control form-control-lg">
                <option>คุณยุ้ย</option>
                <option>คุณโอมห์</option>
                <option>คุณผึ้ง</option>
                <option>คุณเม้ง</option>
              </select>
            </td>`
      cell_idx[4].innerHTML = '<td><input type="date" class="form-control input-task" name="bday" value="2019-11-01"></td>'
      cell_idx[5].innerHTML = '<td><input type="date" class="form-control input-task" name="bday" value="2019-11-01"></td>'
      cell_idx[6].innerHTML = `<td> <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 รอ
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="#">รอ</a>
              <a class="dropdown-item" href="#">กำลังดำเนินการ</a>
              <a class="dropdown-item" href="#">เสร็จสมบูรณ์</a>
            </div>
          </div> 
          </td>`
  }
}

componentProjectTopic()
generateProjectTasks()