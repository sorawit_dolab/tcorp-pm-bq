
function componentFormLogin() {
    document.getElementById("componentFormLogin").innerHTML = `
    <form class="form-signin" action="_project_overview.html">
        <img class="mb-4" src="{{ site.baseurl }}/docs/{{ site.docs_version }}/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
        <h1 id="Vanilla-Team">Vanilla</h1>
        <p class="TopicLogin">เข้าสู่ระบบ</p>
        <label for="inputEmail" class="sr-only">Email address</label>
        <input type="email" id="inputEmail" class="form-control" placeholder="Email address" autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" placeholder="Password">
        <div class="checkbox mb-3 center-text">
            <label class="remember-password">
                <input type="checkbox" value="remember-me"> จดจำรหัสผ่าน
            </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" id="ButtonLogin" type="submit">เข้าสู่ระบบ</button>
        <a href="register-page.html"><p class="DetailLogin">สมัครสมาชิกใหม่</p></a>
        <a href="forget-pass-page.html"><p class="DetailLogin">ลืมรหัสผ่าน</p></a>
        <p class="mt-5 mb-3 text-muted center-text">&copy; 2019 By Vanilla Team</p>
    </form>
    `

}

componentFormLogin()

// # Now don't used