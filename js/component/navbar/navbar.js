function navSidebar() {
    // <!-- Sidebar -->
    themeColorDarkVanilla = "bg-dark-vanilla"
    var nav_sidebar = `
    <div class="`+ themeColorDarkVanilla +` border-right " id="sidebar-wrapper">
      <div class="sidebar-brand">Vanilla</div>
      <div class="list-group list-group-flush">
        <a href="#" class="list-group-item list-group-item-action `+ themeColorDarkVanilla +`"><i class="fas fa-tachometer-alt icon-fas-nav"></i>Dashboard</a>
        <a href="_project_overview.html" class="list-group-item list-group-item-action `+ themeColorDarkVanilla +`"><i class="fas fa-tasks icon-fas-nav"></i>Project Management</a>
        <a href="_product.html" class="list-group-item list-group-item-action `+ themeColorDarkVanilla +`"><i class="fas fa-chart-bar icon-fas-nav"></i>Products</a>
      </div>

      <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Agenda</span>
        <a class="d-flex align-items-center text-muted" href="create-project-detail.html">
          <span><i class="fas fa-plus-circle"></i></span>
        </a>
      </h6>

      <div class="list-group list-group-flush">
        <a href="_project_tasks.html" class="list-group-item list-group-item-action `+ themeColorDarkVanilla +`  sidebar-projects">TOT: งานจ้างเหมาติดตั้งอุปก...</a>
      </div>
    </div>`;
    document.getElementById('navSidebar').innerHTML = nav_sidebar;
}

function navTopbar() {
  // <!-- Sidebar -->
  themeColorWhiteVanilla = "bg-white"
  var nav_topbar = `
  <nav class="navbar navbar-expand-lg navbar-light `+ themeColorWhiteVanilla +`">
      <button class="btn btn-primary" id="menu-toggle">Menu</button>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
          <li class="nav-item active">
            <a class="nav-link" href="_project_overview.html">Projects Management<span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">HR</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Finance</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="_users_management.html">Users Management</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <img src="./images/profile2 crop.jpg" class="rounded-circle" style="width: 40px;">
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="_users_profile.html">โปรไฟท์</a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="index.html">ออกจากระบบ</a>
            </div>
          </li>
        </ul>
      </div>
    </nav>`;
  document.getElementById('navTopbar').innerHTML = nav_topbar;
}


navSidebar();
navTopbar();

// <!-- Menu Toggle Script -->
$("#menu-toggle").click(function(e) {
  e.preventDefault();
  $("#wrapper").toggleClass("toggled");
});