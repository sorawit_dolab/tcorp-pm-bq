function componentUserProfileTopic() {
    document.getElementById("componentUserProfileTopic").innerHTML = `
    <div class="row">
      <div class="col-12">
          <h4 class="T-Corporation-CoLt">T Corporation Co.,Lts</h4>
          <h2 class="Projects line_under_Projects">Profile</h2>
      </div>
  </div>
    `;
}

function componentUserProfileImage() {
    document.getElementById("componentUserProfileImage").innerHTML = `
    <div class="row newRow">
        <div class="col-12">
            <img src="images/profile2 crop.jpg" width="100%">
        </div>
    </div>
    
    `
}

function componentUserProfile() {
    document.getElementById("componentUserProfile").innerHTML = `
    <div class="row">
        <div class="col-12">
            <h2>Sorawit Sirimaleewattana</h2>
            <h4 class="line_under_Projects">ตำแหน่งงาน: Project Management</h4>
            <h3>รายละเอียดเพิ่มเติม</h3>
            <h5>Email: sr.sorawit@gmail.com</h5>
            <h5>เบอร์ติดต่อ: 094-660-1343</h5>
            <h5>เพศ: ชาย</h5>
            <h5>วันเกิด: 03-09-40</h5>
            <h5>ที่อยู่: บ้านพักรถไฟ กม.11 เขตจตุจักร แขวงจตุจักร กรุงเทพมหานคร 10900</h5>
            <button type="button" class="btn btn-secondary">แก้ไขข้อมูลส่วนตัว</button>
        </div>
    </div>
    `
}

componentUserProfileImage()
componentUserProfile()
componentUserProfileTopic()
