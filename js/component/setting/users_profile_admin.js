function componentUserProfileAdminTopic() {
    document.getElementById("componentUserProfileAdminTopic").innerHTML = `
    <div class="row">
      <div class="col-12">
          <h4 class="T-Corporation-CoLt">T Corporation Co.,Lts</h4>
          <h2 class="Projects line_under_Projects">Users Management</h2>
      </div>
  </div>
    `;
}

function componentUserProfileAdminImage() {
    document.getElementById("componentUserProfileAdminImage").innerHTML = `
    <div class="row newRow">
        <div class="col-12">
            <img src="images/profile2 crop.jpg" width="100%">
        </div>
    </div>
    `
}

function componentUserProfileAdmin() {
    document.getElementById("componentUserProfileAdmin").innerHTML = `
    <div class="row">
        <div class="col-12">
            <h2>Sorawit Sirimaleewattana</h2>
            <h4>ตำแหน่งงาน:</h4>
            <div class="row">
                <div class="col-6">
                <select class="form-control">
                    <option selected>Project Management</option>
                    <option>Finance</option>
                    <option>Stock</option>
                </select>
                </div>
                <div class="col-6">
                    <form action="_users_management.html">
                        <button type="submit" class="btn btn-success">บันทึก</button>
                    </form>
                </div>
            </div>
            

                <h3>รายละเอียดเพิ่มเติม</h3>
                <h5>Email: sr.sorawit@gmail.com</h5>
                <h5>เบอร์ติดต่อ: 094-660-1343</h5>
                <h5>เพศ: ชาย</h5>
                <h5>วันเกิด: 03-09-40</h5>
                <h5>ที่อยู่: บ้านพักรถไฟ กม.11 เขตจตุจักร แขวงจตุจักร กรุงเทพมหานคร 10900</h5>


        </div>
    </div>
    `
}


componentUserProfileAdminTopic()
componentUserProfileAdminImage()
componentUserProfileAdmin()
