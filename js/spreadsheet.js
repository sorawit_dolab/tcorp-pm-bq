
var dragObjectRowIndex = -1;
var dragObject;

function drag(ev) {
  dragObject = ev;
  dragRowIndex = ev.target.parentElement.rowIndex;
  console.log("moveFromRow", dragRowIndex);
  console.log("dragObject", dragObject);
}

function allowDrop(ev) {
  ev.preventDefault();
}

function drop(ev) {
  ev.preventDefault();
  var dropToRow = ev.target.parentElement.rowIndex;

  console.log("dragObject.target.parentElement.childNodes[3].childNodes[0]", dragObject.target.parentElement.childNodes[3].childNodes[0]);
  console.log("dragObject.target.parentElement.childNodes[3].childNodes[0]", dragObject.target.parentElement);
  classNameOfDrop = dragObject.target.parentElement.childNodes[3].childNodes[0];

  console.log("ev:", ev)
  console.log("dropToRow:", dropToRow)
  // console.log("classNameOfDrop:", classNameOfDrop)


  // var taskRow = table.getElementsByTagName("tr");
  
  var table = document.getElementById("display-table");
  var taskRow = table.getElementsByTagName("tr");
  

  if (classNameOfDrop === 'fas fa-caret-right icon-movement'){
    console.log("className: right")
    // Delete Row Section
    document.getElementById("display-table").deleteRow(dragRowIndex);
    nextTask = 1
    console.log("taskRow.length", taskRow.length)
    console.log("dragRowIndex", dragRowIndex)
    for (var i=1; i<taskRow.length; i++) {
      console.log("className: right :i:", i)
      // console.log("className: right :moveToRow:", moveToRow)
      var hearderClassName = taskRow[i].childNodes[3].childNodes[0];
      if (i >= dragRowIndex){
          // console.log(">>>rowSeleted_idx+1", rowSeleted_idx+1)
          // console.log(">>> i", i, taskRow[i].cells[2])
          
          // document.getElementById("display-table").deleteRow(i);
          if (hearderClassName != null) {
            if (hearderClassName.className === 'fas fa-caret-right icon-movement'){
              console.log("??? break right", i)
              break;
            }
            else if (hearderClassName.className === 'fas fa-caret-down icon-movement') {
              console.log("??? break down", i)
              break;
            }
          }
          console.log("taskRow[i]", taskRow[i])
          console.log("nextTask", nextTask)
          if (nextTask === 1){
            // taskRow[i].style.display = '';
            moveTask(dragObject, dropToRow, table)
            document.getElementById("display-table").deleteRow(i);
          }
          else {
            console.log("MM")
            // taskRow[i].style.display = '';
            // document.getElementById("display-table").deleteRow(i);
            console.log(">>> Final")

          }
          nextTask += 1
          // // Delete Row Sub Task
          // document.getElementById("display-table").deleteRow(i);
          
          

          // taskRow[i].style.display = 'none';
      }
    }

  }
  else{
    console.log("className: down")
    // Delete Row
    document.getElementById("display-table").deleteRow(dragRowIndex);
    moveTask(dragObject, dropToRow, table)

  }
}

function moveTask(dragObjectTask, dropRowIndex, table) {
  // Insert Row
  var row = table.insertRow(dropRowIndex);
  var cell = row.insertCell(0);
  cell.colSpan = "5";

  // Set Attribute
  row.draggable = true;
  row.ondragstart = function(e){drag(e)};
  row.ondrop = function(e){drop(e)};
  row.ondragover = function(e){allowDrop(e)};
  
  // Check and Set Col.
  console.log("===", dragObjectTask)
  var cell_idx = []
  try {
    for (i = 0; i < dragObjectTask.target.parentElement.cells.length; i++) {
      cell_idx.push(row.insertCell(i))
    }
  
    // Fill data into rows
    for (i = 0; i < dragObjectTask.target.parentElement.cells.length; i++) {
      var objCells = dragObjectTask.target.parentElement.cells.item(i);
      cell_idx[i].innerHTML = objCells.innerHTML;
    }
  }
  catch(err) {
    for (i = 0; i < dragObjectTask.target.srcElement.cells.length; i++) {
      cell_idx.push(row.insertCell(i))
    }
  
    // Fill data into rows
    for (i = 0; i < dragObjectTask.target.srcElement.cells.length; i++) {
      var objCells = dragObjectTask.target.srcElement.cells.item(i);
      cell_idx[i].innerHTML = objCells.innerHTML;
    }
  }
  
}

function moveSubTask(dragObjectTask, dropRowIndex, table) {
  // Insert Row
  console.log("dropRowIndex", dropRowIndex)
  var row = table.insertRow(dropRowIndex);
  var cell = row.insertCell(0);
  cell.colSpan = "5";

  // Set Attribute
  row.draggable = true;
  row.ondragstart = function(e){drag(e)};
  row.ondrop = function(e){drop(e)};
  row.ondragover = function(e){allowDrop(e)};
  
  // Check and Set Col.
  var cell_idx = []
  for (i = 0; i < dragObjectTask.cells.length; i++) {
    cell_idx.push(row.insertCell(i))
  }

  // Fill data into rows
  for (i = 0; i < dragObjectTask.cells.length; i++) {
    var objCells = dragObjectTask.cells.item(i);
    cell_idx[i].innerHTML = objCells.innerHTML;
  }
}